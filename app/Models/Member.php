<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'member';

    public function provider()
    {
        return $this->belongsToMany(Provider::class)
            ->using(MemberProvider::class)
            ->withPivot('no_polis', 'masa_berlaku');
    }
}
