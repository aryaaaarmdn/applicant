<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Claim extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'claim';
    protected $with = ['member_provider.member', 'member_provider.provider'];

    public function member_provider()
    {
        return $this->belongsTo(MemberProvider::class, 'member_provider_id');
    }

    // public function member()
    // {
    //     return $this->hasOneThrough(
    //         Member::class,
    //         MemberProvider::class,
    //         'member_provider.member_id',
    //         'member.id',
    //         'id',
    //         'member_provider.id'
    //     );
    // }

    // public function provider()
    // {
    //     return $this->hasOneThrough(
    //         Provider::class,
    //         MemberProvider::class,
    //         'provider_id',
    //         'id'
    //     );
    // }
}
