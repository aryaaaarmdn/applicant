<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'provider';

    public function member()
    {
        return $this->belongsToMany(Member::class)
            ->using(MemberProvider::class)
            ->withPivot('no_polis', 'masa_berlaku');
    }
}
