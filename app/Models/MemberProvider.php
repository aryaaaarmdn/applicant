<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class MemberProvider extends Pivot
{
    protected $guarded = [];
    protected $table = 'member_provider';

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }
}
