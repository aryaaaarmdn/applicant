<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MemberResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'nama' => $this->nama,
            'NIK' => $this->NIK,
            'alamat' => $this->alamat,
            'is_active' =>
            $this->is_active . ' (' . ($this->is_active == 0 ? 'Tidak Aktif' : 'Aktif') . ')',
        ];
    }
}
