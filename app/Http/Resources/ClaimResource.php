<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\MemberResource;

class ClaimResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'member' => new MemberResource($this->member_provider->member),
            'claim' => [
                'provider' => $this->member_provider->provider->nama,
                'is_accept' =>
                $this->is_accepted . ' (' . ($this->is_accepted != 2 ? 'Menunggu Konfirmasi / Ditolak' : 'Diterima') . ')',
                'surat_jaminan' => $this->surat_jaminan,
                'no_polis' => $this->member_provider->no_polis,
                'keluhan' => $this->member_provider->keluhan,
                'biaya' => $this->member_provider->biaya,
                'masa_berlaku' => $this->member_provider->masa_berlaku,
            ]
        ];
    }
}
