<?php

namespace App\Http\Controllers;

use App\Models\Member;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    public function __invoke()
    {
        $member = Member::with('provider')->get();
        return view('member', [
            'member' => $member,
        ]);
    }
}
