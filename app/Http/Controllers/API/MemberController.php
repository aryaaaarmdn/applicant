<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Member;
use App\Http\Resources\MemberResource;

class MemberController extends Controller
{
    public function getAll()
    {
        return response()->json([
            'status' => 200,
            'data' => MemberResource::collection(Member::all())
        ]);
    }

    public function getById(Member $id)
    {
        return response()->json([
            'status' => 200,
            'data' => new MemberResource($id),
        ]);
    }
}
