<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Claim;
use App\Http\Resources\ClaimResource;

class ClaimController extends Controller
{
    public function getAllData()
    {
        $data = Claim::with('member_provider.member', 'member_provider.provider')->get();
        return response()->json([
            'status' => 200,
            'data' => ClaimResource::collection($data)
        ]);
    }

    public function getById(Claim $id)
    {
        return response()->json([
            'status' => 200,
            'data' => new ClaimResource($id)
        ]);
    }
}
