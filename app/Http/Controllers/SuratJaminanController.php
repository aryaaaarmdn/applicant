<?php

namespace App\Http\Controllers;

use App\Models\Claim;

class SuratJaminanController extends Controller
{
    public function __invoke()
    {
        $data = Claim::with('member_provider.member', 'member_provider.provider')
            ->where('is_accepted', 2)
            ->get();

        return view('surat-jaminan', [
            'data' => $data,
        ]);
    }
}
