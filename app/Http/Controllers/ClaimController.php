<?php

namespace App\Http\Controllers;

use App\Models\Claim;
use App\Models\Member;

class ClaimController extends Controller
{
    public function __invoke()
    {
        $data_klaim = Claim::with('member_provider.member', 'member_provider.provider')->get();
        $data_member = Member::all();

        return view('klaim', [
            'data_klaim' => $data_klaim,
            'data_member' => $data_member,
        ]);
    }
}
