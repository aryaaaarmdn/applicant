<?php

namespace App\Http\Livewire;

use App\Models\Member as MemberModel;
use Livewire\Component;

class Member extends Component
{
    protected $listeners = ['memberAdded' => '$refresh'];

    public function render()
    {
        $member = MemberModel::with('provider')->get();
        return view('livewire.member', ['member' => $member]);
    }
}
