<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Claim;
use Livewire\WithFileUploads;

class SuratJaminan extends Component
{
    use WithFileUploads;

    public Claim $data;
    public $surat;

    public function render()
    {
        return view('livewire.surat-jaminan');
    }

    public function save()
    {
        $this->validate([
            'surat' => 'required|file|max:2048|mimes:pdf,doc,docx',
        ], [
            'surat.required' => 'Surat tidak boleh kosong',
            'surat.file' => 'Surat harus berbentuk dokumen',
            'surat.max' => 'Ukuran Surat Tidak Boleh Lebih dari :max kilobytes',
            'surat.mimes' => 'Tipe file surat harus salah satu dari :values'
        ]);

        $this->surat->store('surat');
        $this->data->surat_jaminan = 'surat/' . $this->surat->hashName();
        $this->data->save();

        $this->dispatchBrowserEvent(
            'toast',
            [
                'message' => 'Berhasil Mengeluarkan Surat Jaminan',
            ]
        );
    }
}
