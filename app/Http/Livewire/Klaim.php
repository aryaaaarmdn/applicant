<?php

namespace App\Http\Livewire;

use App\Models\Claim;
use Livewire\Component;

class Klaim extends Component
{
    public Claim $data;
    public $biaya;

    public function mount()
    {
        $this->biaya = $this->data->member_provider->biaya;
    }

    public function render()
    {
        return view('livewire.klaim');
    }

    public function setStatus($status)
    {
        if (!$this->biaya && $this->data->is_accepted != 2) {
            $msg = 'Berikan Estimasi Biaya Terlebih Dahulu';
        } else {

            if ($status == 1) {
                $this->data->member_provider->biaya = null;
                $this->biaya = null;
            } elseif ($status == 2) {
                $this->data->member_provider->biaya = $this->biaya;
            }

            $this->data->is_accepted = $status;
            $this->data->member_provider->save();
            $this->data->save();
            $msg = $status == 1 ? 'Klaim Berhasil Ditolak' : 'Berhasil Terima Klaim';
        }

        $this->dispatchBrowserEvent(
            'toast',
            [
                'message' => $msg,
            ]
        );
    }
}
