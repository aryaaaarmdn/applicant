<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithFileUploads;

use App\Imports\MemberImport;
use Maatwebsite\Excel\Facades\Excel;

class Action extends Component
{
    use WithFileUploads;

    public $addByFileStatus = 0;

    public $fileAdd = null;

    public function render()
    {
        return view('livewire.action');
    }

    public function addByFile($status)
    {
        if ($status != 1) {
            $this->resetErrorBag();
            $this->resetValidation();
        }
        $this->addByFileStatus = $status;
    }

    public function submitAddByFile()
    {
        $this->validate([
            'fileAdd' => 'required|file|max:2048|mimes:csv,xlsx,xls',
        ], [
            'fileAdd.required' => 'File tidak boleh kosong',
            'fileAdd.file' => 'File harus berbentuk dokumen',
            'fileAdd.max' => 'Ukuran File Tidak Boleh Lebih dari :max kilobytes',
            'fileAdd.mimes' => 'Tipe file File harus salah satu dari :values'
        ]);

        Excel::import(new MemberImport(), $this->fileAdd);
        $this->addByFileStatus = 0;
        $this->fileAdd = null;
        $this->dispatchBrowserEvent(
            'toast',
            [
                'message' => 'Berhasil Menambahkan Data',
            ]
        );
        $this->emit('memberAdded');
    }
}
