<?php

namespace App\Imports;

use App\Models\Member;
use App\Models\Provider;
use App\Models\MemberProvider;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class MemberImport implements ToCollection
{
    private $member_all, $provider_all;

    public function __construct()
    {
        $this->member_all = Member::all();
        $this->provider_all = Provider::all();
    }
    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        foreach ($collection as $index => $value) {
            if ($index == 0) continue;

            // Member
            if ($value[3] == 'aktif') {
                $aktif = 1;
            } elseif ($value[3] == 'tidak aktif') {
                $aktif = 0;
            }

            $member = Member::updateOrCreate(
                ['NIK' => $value[1]],
                [
                    'nama' => $value[0],
                    'alamat' => $value[2],
                    'is_active' => $aktif
                ]
            );

            // Provider
            $provider = $this->provider_all
                ->where('nama', $value[6])->first();

            // $provider2 = $member->provider()->pluck('provider_id')->toArray();
            // $provider2[] = $provider->id;

            // $member->provider()->syncWithPivotValues(array_unique($provider2), [
            //     'no_polis' => $value[4],
            //     'masa_berlaku' => $value[5]
            // ], false);

            $member->provider()->syncWithPivotValues($provider->id, [
                'no_polis' => $value[4],
                'masa_berlaku' => $value[5]
            ], false);
        }
    }
}
