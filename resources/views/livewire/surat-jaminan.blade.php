<tr>
    <td>{{ $data->member_provider->member->nama }}</td>
    <td>{{ $data->member_provider->provider->nama }}</td>
    <td>{{ $data->member_provider->no_polis }}</td>
    <td>{{ $data->member_provider->keluhan }}</td>
    <td>{{ $data->member_provider->biaya }}</td>
    <td>{{ $data->member_provider->masa_berlaku }}</td>
    <td>
        @if($data->surat_jaminan)
            <a href="{{ asset('storage/' . $data->surat_jaminan) }}" 
                target="_blank">
                <i class="far fa-fw fa-file"></i>
            </a>
        @else
            <form wire:submit.prevent="save">
                <div class="d-flex justify-content-around">
                    <input type="file" wire:model="surat">
                    @error('surat') 
                        <div>
                            <span class="text-danger text-sm">
                                {{ $message }}
                            </span> 
                        </div>
                    @enderror
                    <button class="btn btn-sm btn-primary" type="submit">Upload</button>
                </div>
            </form>
        @endif
    </td>
</tr>