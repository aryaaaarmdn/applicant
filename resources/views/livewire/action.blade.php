<div class="row">
    <div class="col-3">
        <div class="row">
            <div class="col-12">
                @if($addByFileStatus == 0)
                    <button class="btn btn-sm btn-info" wire:click="addByFile(1)">
                        Tambah/Edit Data Member (CSV/Excel)
                    </button>
                @else
                    <button class="btn btn-sm btn-danger" wire:click="addByFile(0)">
                        Batal
                    </button>
                @endif
            </div>
            <div class="col-12">
                @if($addByFileStatus != 0)
                    <form wire:submit.prevent="submitAddByFile">
                        <input type="file" class="form-control" wire:model="fileAdd">
                        @error('fileAdd') 
                            <div>
                                <span class="text-danger text-sm">
                                    {{ $message }}
                                </span> 
                            </div>
                        @enderror
                        <button class="btn btn-sm btn-primary">Simpan</button>
                    </form>
                @endif
            </div>
        </div>
    </div>
</div>