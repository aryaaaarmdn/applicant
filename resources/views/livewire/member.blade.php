<div class="table-responsive mt-3">
    <h5>Data Member</h5>
    <table id="member" class="table table-bordered">
        <thead>
            <tr>
                <th>Nama</th>
                <th>NIK</th>
                <th>No Polis</th>
                <th>Status</th>
                <th>Masa Berlaku</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($member as $_member)
                <tr>
                    <td>{{ $_member->nama }}</td>
                    <td>{{ $_member->NIK }}</td>
                    <td style="margin:0; padding:0">
                        <table class="table table-borderless" 
                            style="margin:0; padding:0">
                            @foreach ($_member->provider as $mp)
                                <tr>
                                    <td> - {{ $mp->pivot->no_polis }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </td>
                    <td>
                        <div class="row">
                            @if($_member->is_active != 0)
                                <span class="badge badge-primary">
                                    Aktif
                                </span>
                            @else
                                <span class="badge badge-danger">
                                    Tidak Aktif
                                </span>
                            @endif
                        </div>
                    </td>
                    <td style="margin:0; padding:0">
                        <table class="table table-borderless" 
                            style="margin:0; padding:0">
                            @foreach ($_member->provider as $mp)
                                <tr>
                                    <td> - {{ $mp->pivot->masa_berlaku }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </td>
                </tr>
            @empty
                <tr class="text-center">
                    <td colspan="5">
                        <i><b>Tidak ada data</b></i>
                    </td>
                </tr>
            @endforelse
        </tbody>
    </table>
</div>