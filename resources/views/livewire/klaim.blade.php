<tr>
    <td>{{ $data->member_provider->member->nama }}</td>
    <td>{{ $data->member_provider->provider->nama }}</td>
    <td>{{ $data->member_provider->no_polis }}</td>
    <td>{{ $data->member_provider->keluhan }}</td>
    <td>
        <input type="text" class="form-control"
            wire:model="biaya"
            value={{ $data->member_provider->biaya }}
        >
    </td>
    <td>{{ $data->member_provider->masa_berlaku }}</td>
    <td>
        @if($data->is_accepted == 0)
            <span class="badge badge-primary">Menunggu Konfirmasi</span>
        @elseif ($data->is_accepted == 1)
            <span class="badge badge-danger">Ditolak</span>
        @else
            <span class="badge badge-success">Diterima</span>
        @endif
    </td>
    <td>
        @if($data->is_accepted == 0 || $data->is_accepted == 1)
            <button class="btn btn-sm btn-primary"
                wire:click="setStatus(2)"
            >
                Terima
            </button>
        @else
            <button class="btn btn-sm btn-danger"
                wire:click="setStatus(1)"
            >
                Tolak
            </button>
        @endif
    </td>
</tr>