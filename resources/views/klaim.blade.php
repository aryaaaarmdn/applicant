@extends('adminlte::page')

@section('title', 'Data Klaim')

@section('content_header')
    <h1 class="m-0 text-dark">Data Klaim</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table responsive mb-2">
                        <h5>Data Member</h5>
                        <table id="member">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>NIK</th>
                                    <th>Alamat</th>
                                    <th>Aktif</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($data_member as $member)
                                    <tr>
                                        <td>{{ $member->nama }}</td>
                                        <td>{{ $member->NIK }}</td>
                                        <td>{{ $member->alamat }}</td>
                                        <td>
                                            @if($member->is_active == 0)
                                                <span class="badge badge-danger">Tidak Aktif</span>
                                            @else
                                                <span class="badge badge-success">Aktif</span>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr class="text-center">
                                        <td colspan="4">
                                            <i><b>Tidak ada data</b></i>
                                        </td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <h5>Data Klaim</h5>
                        <table id="klaim" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Member</th>
                                    <th>Provider</th>
                                    <th>No Polis</th>
                                    <th>Keluhan</th>
                                    <th>Biaya</th>
                                    <th>Masa Berlaku</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($data_klaim as $_data)
                                    @livewire('klaim', [
                                        'data' => $_data
                                    ], key($_data->id))
                                @empty
                                    <tr class="text-center">
                                        <td colspan="6">
                                            <i><b>Tidak ada data</b></i>
                                        </td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        window.addEventListener('toast', event => {
            alert(event.detail.message);
        });

        $(document).ready( function () {
            $('#member').DataTable();
            $('#klaim').DataTable();
        } );
    </script>
@stop
