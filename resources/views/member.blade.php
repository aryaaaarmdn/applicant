@extends('adminlte::page')

@section('title', 'Data Member')

@section('content_header')
    <h1 class="m-0 text-dark">Member</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @livewire('action')
                    @livewire('member')
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        window.addEventListener('toast', event => {
            alert(event.detail.message);
        });

        // $(document).ready( function () {
        //     $('#member').DataTable();
        //     $('#klaim').DataTable();
        // } );
    </script>
@stop