@extends('adminlte::page')

@section('title', 'Surat Jaminan')

@section('content_header')
    <h1 class="m-0 text-dark">Surat Jaminan</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <h5>Data Klaim</h5>
                        <table id="klaim" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Member</th>
                                    <th>Provider</th>
                                    <th>No Polis</th>
                                    <th>Keluhan</th>
                                    <th>Biaya</th>
                                    <th>Masa Berlaku</th>
                                    <th>Surat Jaminan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($data as $_data)
                                    @livewire('surat-jaminan', [
                                        'data' => $_data
                                    ])
                                @empty
                                    <tr class="text-center">
                                        <td colspan="6">
                                            <i><b>Tidak ada data</b></i>
                                        </td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        window.addEventListener('toast', event => {
            alert(event.detail.message);
        });

        $(document).ready( function () {
            $('#member').DataTable();
            $('#klaim').DataTable();
        } );
    </script>
@stop
