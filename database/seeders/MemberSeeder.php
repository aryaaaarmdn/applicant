<?php

namespace Database\Seeders;

use App\Models\Member;
use Illuminate\Database\Seeder;

class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $items = [
            [
                'id' => 1,
                'nama' => 'Member 1',
                'NIK' => '12345',
                'alamat' => 'Jakarta',
                'is_active' => 1,
            ],
            [
                'id' => 2,
                'nama' => 'Member 2',
                'NIK' => '23456',
                'alamat' => 'Bandung',
                'is_active' => 1,
            ],
            [
                'id' => 3,
                'nama' => 'Member 3',
                'NIK' => '34567',
                'alamat' => 'Bogor',
                'is_active' => 0,
            ],
            [
                'id' => 4,
                'nama' => 'Member 4',
                'NIK' => '45678',
                'alamat' => 'Depok',
                'is_active' => 1,
            ],
            [
                'id' => 5,
                'nama' => 'Member 5',
                'NIK' => '56789',
                'alamat' => 'Bekasi',
                'is_active' => 0,
            ],
        ];

        foreach ($items as $item) {
            Member::updateOrCreate(
                ['nama' => $item['nama'], 'NIK' => $item['NIK']],
                ['alamat' => $item['alamat'], 'is_active' => $item['is_active']]
            );
        }
    }
}
