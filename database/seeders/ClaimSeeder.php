<?php

namespace Database\Seeders;

use App\Models\Claim;
use Illuminate\Database\Seeder;

class ClaimSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $items = [
            [
                'member_provider_id' => 1,
                'is_accepted' => 1,
            ],
            [
                'member_provider_id' => 2,
                'is_accepted' => 1,
            ],
            [
                'member_provider_id' => 3,
                'is_accepted' => 0,
            ],
            [
                'member_provider_id' => 4,
                'is_accepted' => 1,
            ],
            [
                'member_provider_id' => 5,
                'is_accepted' => 0,
            ],
            [
                'member_provider_id' => 6,
                'is_accepted' => 0,
            ],
        ];

        foreach ($items as $item) {
            Claim::updateOrCreate(
                ['member_provider_id' => $item['member_provider_id']],
                $item
            );
        }
    }
}
