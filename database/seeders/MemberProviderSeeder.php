<?php

namespace Database\Seeders;

use App\Models\MemberProvider;
use Illuminate\Database\Seeder;

class MemberProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $items = [
            [
                'id' => 1,
                'member_id' => 1,
                'provider_id' => 1,
                'no_polis' => '123',
                'keluhan' => 'Sakit Kepala',
                'masa_berlaku' => '2026-01-01',
            ],
            [
                'id' => 2,
                'member_id' => 2,
                'provider_id' => 2,
                'no_polis' => '234',
                'keluhan' => 'Sakit Perut',
                'masa_berlaku' => '2026-01-01',
            ],
            [
                'id' => 3,
                'member_id' => 3,
                'provider_id' => 3,
                'no_polis' => '345',
                'keluhan' => 'Sakit Hati',
                'masa_berlaku' => '2026-01-01',
            ],
            [
                'id' => 4,
                'member_id' => 4,
                'provider_id' => 4,
                'no_polis' => '456',
                'keluhan' => 'Ketabrak Mobil',
                'masa_berlaku' => '2026-01-01',
            ],
            [
                'id' => 5,
                'member_id' => 5,
                'provider_id' => 5,
                'no_polis' => '567',
                'keluhan' => 'Keracunan',
                'masa_berlaku' => '2026-01-01',
            ],
            [
                'id' => 6,
                'member_id' => 1,
                'provider_id' => 5,
                'no_polis' => '134',
                'keluhan' => 'Patah Tulang',
                'masa_berlaku' => '2026-01-01',
            ],
        ];

        foreach ($items as $item) {
            MemberProvider::updateOrCreate(
                [
                    'member_id' => $item['member_id'],
                    'no_polis' => $item['no_polis'],
                ],
                [
                    'provider_id' => $item['provider_id'],
                    'keluhan' => $item['keluhan'],
                    'masa_berlaku' => $item['masa_berlaku']
                ]
            );
        }
    }
}
