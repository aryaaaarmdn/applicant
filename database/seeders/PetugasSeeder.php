<?php

namespace Database\Seeders;

use App\Models\Petugas;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class PetugasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                'id' => 1,
                'username' => 'petugas',
                'password' => Hash::make('123'),
            ]
        ];

        foreach ($data as $_data) {
            Petugas::updateOrCreate(['id' => $_data['id']], $_data);
        }
    }
}
