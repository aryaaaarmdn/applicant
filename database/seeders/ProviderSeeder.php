<?php

namespace Database\Seeders;

use App\Models\Provider;
use Illuminate\Database\Seeder;

class ProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $items = [
            ['id' => 1, 'nama' => 'RS 1'],
            ['id' => 2, 'nama' => 'RS 2'],
            ['id' => 3, 'nama' => 'RS 3'],
            ['id' => 4, 'nama' => 'RS 4'],
            ['id' => 5, 'nama' => 'RS 5'],
        ];

        foreach ($items as $item) {
            Provider::updateOrCreate(
                ['nama' => $item['nama']],
                ['nama' => $item['nama']]
            );
        }
    }
}
