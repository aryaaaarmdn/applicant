<?php

use Database\Seeders\MemberSeeder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('member', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('NIK');
            $table->string('alamat');
            $table->integer('is_active');
            $table->timestamps();
        });

        Artisan::call('db:seed', [
            '--class' => MemberSeeder::class
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('member');
    }
};
