<?php

use App\Models\MemberProvider;
use Database\Seeders\MemberProviderSeeder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('member_provider', function (Blueprint $table) {
            $table->id();
            $table->foreignId('member_id')
                ->constrained('member');
            $table->foreignId('provider_id')
                ->constrained('provider');
            $table->string('no_polis')->nullable();
            $table->string('keluhan')->nullable();
            $table->integer('biaya')->nullable();
            $table->date('masa_berlaku')->nullable();
            $table->timestamps();
        });

        Artisan::call('db:seed', [
            '--class' => MemberProviderSeeder::class
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('member_provider');
    }
};
