<?php

use Database\Seeders\ClaimSeeder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('claim', function (Blueprint $table) {
            $table->id();
            $table->foreignId('member_provider_id')
                ->constrained('member_provider')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->integer('is_accepted');
            $table->string('surat_jaminan')->nullable();
            $table->timestamps();
        });

        Artisan::call('db:seed', [
            '--class' => ClaimSeeder::class
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('claim');
    }
};
