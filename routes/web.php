<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthenticateController;
use App\Http\Controllers\ClaimController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\SuratJaminanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::controller(AuthenticateController::class)->group(function () {
    Route::get('login', 'index')
        ->middleware('guest')
        ->name('login.view');

    Route::post('login', 'login')
        ->middleware('guest')
        ->name('login');

    Route::post('logout', 'logout')
        ->middleware('auth')
        ->name('logout');
});

Route::middleware('auth')->group(function () {
    Route::view('/dashboard', 'home')->name('dashboard');
    Route::get('/data-klaim', ClaimController::class)->name('klaim');
    Route::get('/surat-jaminan', SuratJaminanController::class)->name('surat-jaminan');
    Route::get('/member', MemberController::class)->name('member');
});
