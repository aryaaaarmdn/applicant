<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\ClaimController;
use App\Http\Controllers\API\MemberController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(MemberController::class)->group(function () {
    Route::get('member', 'getAll');
    Route::get('member/{id}', 'getById');
});

Route::controller(ClaimController::class)->group(function () {
    Route::get('klaim', 'getAllData');
    Route::get('klaim/{id}', 'getById');
});
